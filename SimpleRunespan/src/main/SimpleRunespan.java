package main;

import java.util.ArrayList;
import java.util.List;

import main.runespan.lower.EnumRunespanLower;
import main.task.Task;
import main.task.tasks.TaskCollectFragments;
import main.task.tasks.TaskSiphon;

import org.powerbot.script.PollingScript;
import org.powerbot.script.rt6.ClientContext;

@SimpleRunespan.Manifest(name = "Simple Runespan", description = "A Scriptwriter Application That Automates Runespan")
public class SimpleRunespan extends PollingScript<ClientContext> {

	private List<Task<ClientContext>> taskChain = new ArrayList<Task<ClientContext>>();
	public static Console console;
	
	public SimpleRunespan() {
		console = new Console();
		console.show();
	}
	
	@Override
	public void start() {
		taskChain.add(new TaskCollectFragments(ctx));
		taskChain.add(new TaskSiphon(ctx, new EnumRunespanLower()));
	}
	
	@Override
	public void poll() {
		for(Task t : taskChain) {
			if(t.activate()) {
				t.execute();
			}
		}
	}
}
