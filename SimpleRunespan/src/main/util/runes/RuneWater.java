package main.util.runes;

import main.util.Rune;

public class RuneWater extends Rune {

	public RuneWater(int quantity) {
		super("Water Rune", 24214, quantity);
	}
}
