package main.util.runes;

import main.util.Rune;

public class RuneBody extends Rune {

	public RuneBody(String name, int id, int quantity) {
		super("Body Rune", 24218, quantity);
	}
}
