package main.util.runes;

import main.util.Rune;

public class RuneMind extends Rune {

	public RuneMind(String name, int id, int quantity) {
		super("Mind Rune", 24217, quantity);
	}
}
