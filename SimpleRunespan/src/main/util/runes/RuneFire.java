package main.util.runes;

import main.util.Rune;

public class RuneFire extends Rune {

	public RuneFire(int quantity) {
		super("Fire Rune", 24213, quantity);
	}
}
