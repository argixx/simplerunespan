package main.util.runes;

import main.util.Rune;

public class RuneAir extends Rune {

	public RuneAir(int quantity) {
		super("Air Rune", 24215, quantity);
	}
}
