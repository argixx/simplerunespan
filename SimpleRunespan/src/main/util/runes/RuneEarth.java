package main.util.runes;

import main.util.Rune;

public class RuneEarth extends Rune {

	public RuneEarth(int quantity) {
		super("Earth Rune", 24216, quantity);
	}

}
