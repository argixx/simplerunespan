package main.util;

public class Rune {

	protected String name;
	protected int id;
	protected int quantity;
	
	public Rune(String name, int id, int quantity) {
		this.name = name;
		this.id = id;
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
