package main.runespan.lower;

import main.runespan.RunespanEnum;
import main.util.Rune;
import main.util.runes.RuneAir;
import main.util.runes.RuneEarth;
import main.util.runes.RuneFire;
import main.util.runes.RuneWater;

public class EnumRunespanLower extends RunespanEnum {

	public enum Creatures {
		AIR_ESSLING("Air Essling", 1),
		MIND_ESSLING("Mind Essling", 1),
		WATER_ESSLING("Water Essling", 5),
		EARTH_ESSLING("Earth Essling", 9),
		FIRE_ESSLING("Fire Essling", 14);
		
		public String name;
		public int minLevel;
		private Creatures(String name, int minLevel) {
			this.name = name;
			this.minLevel = minLevel;
		}
	}
	
	public enum Nodes {
		CYCLONE("Cyclone", 1),
		MIND_STORM("Mind Storm", 1),
		WATER_POOL("Water Pool", 5),
		ROCK_FRAGMENT("Rock Fragment", 9),
		FIREBALL("Fireball", 14),
		VINE("Vine", 17);
		
		public String name;
		public int minLevel;
		private Nodes(String name, int minLevel) {
			this.name = name;
			this.minLevel = minLevel;
		}
	}
	
	public enum Platforms {
		FLOAT_PLATFORM(1, new RuneAir(1)),
		EARTH_PLATFORM(9, new RuneEarth(1)),
		ICE_PLATFORM(15, new RuneAir(1), new RuneWater(1)),
		SMALL_MISSILE(20, new RuneAir(1), new RuneEarth(1), new RuneWater(1), new RuneFire(1));
		
		public int minLevel;
		public Rune[] runes;
		private Platforms(int minLevel, Rune... runes) {
			this.minLevel = minLevel;
			this.runes = runes;
		}
	}
}
