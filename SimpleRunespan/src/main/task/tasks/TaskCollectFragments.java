package main.task.tasks;

import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.Npc;

import main.SimpleRunespan;
import main.task.Task;

public class TaskCollectFragments extends Task<ClientContext> {

	private int essenceId = 24227;
	
	public TaskCollectFragments(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		return ctx.players.local().animation() == -1
				&& ctx.backpack.select().id(essenceId).count() <= 0;
	}

	@Override
	public void execute() {
		SimpleRunespan.console.log("Collecting Essence");
		Npc floatingEssence = ctx.npcs.select().name("Floating Essence").poll();
		
		if(floatingEssence.inViewport()) {
			floatingEssence.interact("Collect");
		} else {
			ctx.camera.turnTo(floatingEssence);
			ctx.movement.step(floatingEssence);
		}
	}
}
