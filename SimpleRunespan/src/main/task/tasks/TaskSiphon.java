package main.task.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.GameObject;
import org.powerbot.script.rt6.Npc;

import main.SimpleRunespan;
import main.runespan.RunespanEnum;
import main.runespan.lower.EnumRunespanLower;
import main.task.Task;

public class TaskSiphon extends Task<ClientContext> {

	private RunespanEnum floor;
	private int essenceId = 24227;
	
	public TaskSiphon(ClientContext ctx, RunespanEnum floor) {
		super(ctx);
		this.floor = floor;
	}

	@Override
	public boolean activate() {
		return ctx.players.local().animation() == -1 &&
				ctx.backpack.select().id(essenceId).count() >= 1;
	}

	@Override
	public void execute() {
		SimpleRunespan.console.log("Siphoning...");
		if(floor instanceof EnumRunespanLower) {
			SimpleRunespan.console.log("Siphoning On Lower Level...");
			EnumRunespanLower runespanData = (EnumRunespanLower)floor;
			List<String> nodes = new ArrayList<String>();
			
			for(EnumRunespanLower.Nodes node : EnumRunespanLower.Nodes.values()) {
				if(ctx.skills.level(ctx.skills.RUNECRAFTING) >= node.minLevel) {
					nodes.add(node.name);
				}
			}
			
			if(!ctx.objects.nearest().name(nodes.toArray(new String[nodes.size()])).isEmpty()) {
				GameObject node = ctx.objects.nearest().name(nodes.toArray(new String[nodes.size()])).poll();
				
				if(node.inViewport()) {
					SimpleRunespan.console.log("Siphoning " + node.name());
					node.interact("Siphon");
				} else {
					ctx.camera.turnTo(node);
					ctx.movement.step(node);
				}
			} else {
				List<String> creatures = new ArrayList<String>();
				
				for(EnumRunespanLower.Creatures creature : EnumRunespanLower.Creatures.values()) {
					if(ctx.skills.level(ctx.skills.RUNECRAFTING) >= creature.minLevel) {
						creatures.add(creature.name);
					}
				}
				
				if(!ctx.npcs.nearest().name(creatures.toArray(new String[creatures.size()])).isEmpty()) {
					Npc creature = ctx.npcs.nearest().name(creatures.toArray(new String[creatures.size()])).poll();
					
					if(creature.inViewport()) {
						SimpleRunespan.console.log("Siphoning " + creature.name());
						creature.interact("Siphon");
					} else {
						ctx.camera.turnTo(creature);
						ctx.movement.step(creature);
					}
				} else {
					//Island hop
				}
			}
		}
	}
}
