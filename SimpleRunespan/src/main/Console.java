package main;

import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Console {

	public JFrame frame;
	public JTextArea debugInfo;
	public String newLine = "\n";	
	
	public Console() {
		frame = new JFrame("Developer Console");
		frame.setPreferredSize(new Dimension(600, 350));
		frame.setLocationRelativeTo(null);
		
		debugInfo = new JTextArea(5, 20);
		debugInfo.setEditable(false);
		debugInfo.setLineWrap(true);
		debugInfo.setWrapStyleWord(true);
		debugInfo.setBackground(Color.BLACK);
		debugInfo.setForeground(Color.LIGHT_GRAY);
		debugInfo.setCaretColor(Color.LIGHT_GRAY);
	
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(debugInfo);
		scrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		frame.add(scrollPane);
		
		frame.pack();
		frame.setVisible(false);
		
		log("Main Script Initialized");
	}
	
	public void show() {
		frame.setVisible(true);
	}
	
	public void hide() {
		frame.setVisible(false);
	}
	
	public void log(String message) {
		debugInfo.append("[SYSTEM]: " + message + newLine);
		debugInfo.setCaretPosition(debugInfo.getDocument().getLength());
	}
}
